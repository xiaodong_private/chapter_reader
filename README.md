chapter_reader IDEA 在线&本地阅读插件

**2022/05/13 gitee配置文件被封，QQ群中有本地缓存的hotfix版本，根据之前安装的版本，是否有-high的后缀，安装对应的hotfix版本。下载安装后即可使用本地缓存。（没有缓存的直接在群里让其他道友发一份）** 

# 1. 介绍

- **插件版本 < 2.0.7，旧版配置文件不再受支持，请及时升级最新版。**
- **插件版本 < 2.1.3，对高版本IDEA适配会有问题，请及时升级最新版本**
- 最发布版本【2.1.4】

```
说明："chapter_reader" 是一款IDEA 在线&本地 小说阅读插件
为了帮助各位道友在资本主义的魔爪下，有一个可以调节情绪且可以畅游知识海洋的环境。
"chapter_reader"应运而生。只希望可以帮助更多的人。
(请勿痴迷，该学习则学习，该放松则放松，让加班？不可能！)
希望大家看到这篇文章可以，点赞评论，让更多人知道即可。

遇到问题及时加QQ群，各位道友都会热心帮助的。
插件：chapter_reader
作者：wind
邮件：wind_develop@163.com
QQ群：1群：252192636，2群：251897630（有疑问会有各位道友或作者在线教学）
gitee：https://gitee.com/wind_invade/chapter_reader
博客园：https://www.cnblogs.com/winddniw/p/15689784.html
csdn：https://blog.csdn.net/com_study/article/details/115709454
```



# 2. 安装方式

1. IDEA插件市场直接安装：File | Settings | Plugins | 搜索**chapter_reader**

2. [通过idea插件网下载安装：https://plugins.jetbrains.com/plugin/16544-chapter-reader](https://gitee.com/link?target=https%3A%2F%2Fplugins.jetbrains.com%2Fplugin%2F16544-chapter-reader)



# 3. 使用说明



## 3.1 版本说明（推荐及时升级最新版）

- v2.1.4: 

  1. 新增高版本IDEA适配
  2. 新增远程阅读配置文件读取成功后，缓存到本地。（因gitee误封两次，故加此功能）
  3. x.x.x适用低版本IDEA，x.x.x-high适用高版本IDEA，详见下图

  x.x.x版本(低版本IDEA)：

  ![在这里插入图片描述](https://img-blog.csdnimg.cn/258acf99e5cc4e8e8ae705184c8f29c2.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAd2luZF9zc3Nzc3Nzc3M=,size_20,color_FFFFFF,t_70,g_se,x_16)

  x.x.x-high版本（高版本IDEA）：

  ![在这里插入图片描述](https://img-blog.csdnimg.cn/ad6cf0146dd445a6b6170057df639335.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAd2luZF9zc3Nzc3Nzc3M=,size_20,color_FFFFFF,t_70,g_se,x_16)

- v2.1.2: 
  1. 新增菜单中文转换
  2. 远程阅读配置去除title path
  3. 新增epub本地阅读
  4. 新增远程网站调试，测试节点是否获取正常。
- v2.1.1: check Remote最后会把有效的网站统一打印
- v2.1.0: 1. 修复远程阅读获取内容报错时，堆栈错误。2. 新增关闭插件后，所有快捷键失效（设置窗口除外）



## 3.2 新版配置说明（[chapter_config-v1.json](https://gitee.com/wind_invade/chapter_reader/blob/master/chapter_config-v1.json)）：

注：checkUrl仅是自动检测所用，不是配置完了只有这个网站可以看，chapterPath、contentPath 全站通用的，随意在网站上找自己想看的小说。

- checkUrl = "文章目录"       文章目录 URL（小说目录的URL，仅用于自动检测网站结构是否正确，不是整个网站只能看这一本小说，全站随意选书）
- chapterPath = "文章目录" 文章目录 节点的full path
- contentPath = "章节内容" 章节内容 节点的full path



## 3.3 菜单说明

- IDEA菜单栏 | Tools | Read
- 在插件设置中，可以将菜单设置为中文，这里不再赘述说明。



## 3.4 快捷键设置方法

IDEA | File | Settings | Keymap | Plug-ins | 搜索 Read 自行设置
翻页推荐快捷键，其它自行设置：

- 小键盘"+" "-"(目前最轻松的体验方式)
- 鼠标多功能按键
- 按键 + 鼠标滚轮 (必须是按键+鼠标滚轮, 否则会因优先级的问题, 无法生效)
- 组合快捷键(会很累)




## 3.5 如出现右下角有弹窗提示，操作步骤

File | Settings | Appearance & Behavior | Notifications | 搜索novel  | 将Popup 修改为 No popup



## 3.6 远程阅读FullPath节点获取教程

  - 新增“网站节点调试”界面，调试成功获取后可一键复制配置。
  - 部分网站存在动态生成dom节点的问题，需要使用“保存网页到本地”，打开保存的文件进行FullPath获取。
  - 加入QQ群，寻找群里会添加的道友，帮忙新增。

截图说明：

1. 选择菜单：“保存网站到本地”，选择保存目录

  ![在这里插入图片描述](https://img-blog.csdnimg.cn/b6e02b56ac6c4fb7ba12aaf07373ad34.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAd2luZF9zc3Nzc3Nzc3M=,size_14,color_FFFFFF,t_70,g_se,x_16)

2. 输入小说“章节目录”的URL，稍等一会会弹窗提示。

  

  ![在这里插入图片描述](https://img-blog.csdnimg.cn/4e7e4c3033794fd1843983fab16d6d32.png)

3. 打开保存到本地目录的文件

  ![在这里插入图片描述](https://img-blog.csdnimg.cn/0597d5ecb8e7455fbc70a4d18bd36645.png)

4. 选择文章目录的节点

  ![在这里插入图片描述](https://img-blog.csdnimg.cn/66d24f6e3a8e4d419d146649a1fa0a2d.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAd2luZF9zc3Nzc3Nzc3M=,size_20,color_FFFFFF,t_70,g_se,x_16)

5. 获取对应的FullPath

  ![在这里插入图片描述](https://img-blog.csdnimg.cn/243641b02cb842788771eae3bde42799.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAd2luZF9zc3Nzc3Nzc3M=,size_15,color_FFFFFF,t_70,g_se,x_16)

6. 通过菜单：“网站节点调试”，测试获取的FullPath是否正确。
   ![在这里插入图片描述](https://img-blog.csdnimg.cn/3eddc1abb72f44c98d016499715e92a9.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAd2luZF9zc3Nzc3Nzc3M=,size_15,color_FFFFFF,t_70,g_se,x_16)

7. 解析无问题后，一键复制配置，在gitee chapter_config-v1.json中新增json配置，提交Pull Request（审核通过后，即可阅读该网站，有时会不及时，群中@我）

# 4. 截图演示

## 4.1 菜单展示

![在这里插入图片描述](https://img-blog.csdnimg.cn/8aa7f171afd9469b8e40d4b2591d5e2d.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAd2luZF9zc3Nzc3Nzc3M=,size_20,color_FFFFFF,t_70,g_se,x_16)

## 4.2 左下角展示（需将鼠标焦点触发在IDEA内部窗口中）

![在这里插入图片描述](https://img-blog.csdnimg.cn/a00059a3ef01464daa3b907c08e412c2.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAd2luZF9zc3Nzc3Nzc3M=,size_20,color_FFFFFF,t_70,g_se,x_16)

## 4.3 弹出框展示（需将鼠标焦点放在文件内）

![在这里插入图片描述](https://img-blog.csdnimg.cn/d0867a94033f4b83b14df7c29dd9f226.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAd2luZF9zc3Nzc3Nzc3M=,size_20,color_FFFFFF,t_70,g_se,x_16)



# 5.特此声明（侵权免责声明）

```
无任何商业用途，无任何侵权想法。但如发现侵权或其它问题请 及时 且 成功 与作者本人取得联系。
作者本人会在第一时间进行相关内容的删除。

插件：chapter_reader
作者：wind
邮件：wind_develop@163.com
QQ群：252192636（有疑问会有各位道友或作者在线教学）
博客园：https://www.cnblogs.com/winddniw/p/15689784.html
csdn： https://blog.csdn.net/com_study/article/details/115709454（吃相难看，会渐渐放弃）

在这祝大家工作开心^_^
```

# 如果好用，可以为作者的辛苦进行赞赏。

<div style="display:flex;">
    <img src="https://img-blog.csdnimg.cn/20210713161400215.png" width="49%" height="600px"><img src="https://img-blog.csdnimg.cn/20210713162109319.jpeg"   width="49%" height="500px">
</div>

